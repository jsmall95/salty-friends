# Salty Friends

This is an android application I started working on for fun. I wanted a way to play the popular fighting betting game "Salty Bet" with my friends. The main features of the app are as follows.

## v1.12 - 1/5/2020
- [X] Refactor code for better organization and implement Dagger2 (ugh)
- [X] Call salty bet state API to get current bet state (fighter names, bets locked, etc.)
- [X] Create socket to listen for salty bet update event
- [ ] On bet update, update the fragments for if the event is locked or live and which state its in
- [ ]  Allow Salty Bet API to decide the winners and automate the betting process once players have finalized their bets

## v1.11
- [X] After each player has made their bet, show a summary screen of which players bet for each color
	- [X] Calculate the betting odds based on how much each color received in bets
- [X] Show winner screen
	- [X] This will show the list of players in order of who won the most down to who lost the most
- [X] Keep track of all wins and losses for each player and show a stats screen to show the top players
- [X] Add background track that plays and pauses with the app on screen

## v1.10
- [X] Save all player and bet information in SQLite database '
- [X] Add player icons
- [X]  Add navigation bar icons

## v1.0
- [X] Ability to add/remove players, edit names and total credit
- [X] Allow each player to make a bet for **RED** or **BLUE** and decide how many credits to bet