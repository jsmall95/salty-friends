package com.jsmall.saltyfriends.domain.events;

import android.os.Parcel;
import android.os.Parcelable;

import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;

import java.util.ArrayList;

public class EventModel implements Parcelable {
    private long _id;
    private boolean _finalized;
    private String _redPlayer;
    private String _bluePlayer;
    private String _winner;
    private boolean _active;

    private ArrayList<PlayerBetModel> _playerBets;

    public EventModel(long id, boolean finalized, String redPlayer, String bluePlayer, String winner, boolean active){
        _id = id;
        _finalized = finalized;
        _redPlayer = redPlayer;
        _bluePlayer = bluePlayer;
        _winner = winner;
        _active = active;
    }

    public long getId(){ return _id; }

    public boolean isFinalized() { return _finalized; }
    public void setFinalized(boolean finalized) { _finalized = finalized; }

    public String getRedPlayer(){ return _redPlayer; }
    public void setRedPlayer(String redPlayer) { _redPlayer = redPlayer;}

    public String getBluePlayer(){ return _bluePlayer; }
    public void setBluePlayer(String bluePlayer) { _bluePlayer = bluePlayer;}

    public String getWinner(){ return _winner; }
    public void setWinner(String winner){ _winner = winner; }

    public boolean isActive() { return _active; }
    public void setActive(boolean active) { _active = active; }

    public ArrayList<PlayerBetModel> getPlayerBets(){ return _playerBets; }
    public void setPlayerBets(ArrayList<PlayerBetModel> playerBets){ _playerBets = playerBets; }

    protected EventModel(Parcel in) {
        _id = in.readLong();
        _finalized = in.readByte() != 0x00;
        _redPlayer = in.readString();
        _bluePlayer = in.readString();
        _winner = in.readString();
        _active = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            _playerBets = new ArrayList<PlayerBetModel>();
            in.readList(_playerBets, PlayerBetModel.class.getClassLoader());
        } else {
            _playerBets = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_id);
        dest.writeByte((byte) (_finalized ? 0x01 : 0x00));
        dest.writeString(_redPlayer);
        dest.writeString(_bluePlayer);
        dest.writeString(_winner);
        dest.writeByte((byte) (_active ? 0x01 : 0x00));
        if (_playerBets == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(_playerBets);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<EventModel> CREATOR = new Parcelable.Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };
}