package com.jsmall.saltyfriends.domain.players;

import java.util.ArrayList;

public class ManagePlayersUseCase {
    private PlayerRepository _repo;

    public ManagePlayersUseCase(PlayerRepository playerRepository){
        _repo = playerRepository;
    }

    public ArrayList<PlayerModel> getPlayers(){
        return _repo.getPlayers();
    }

    public PlayerModel getPlayer(long playerId){
        return _repo.getPlayer(playerId);
    }

    public PlayerModel addPlayer(String playerName, int imageResource, int totalCredits){
        return _repo.insertPlayer(playerName, imageResource, totalCredits);
    }

    public PlayerModel updatePlayer(PlayerModel player){
        if(player != null){
            return _repo.updatePlayer(player);
        }
        return null;
    }

    public boolean deletePlayer(PlayerModel player){
        if(player != null){
            return _repo.deletePlayer(player.getId());
        }
        return false;
    }
}
