package com.jsmall.saltyfriends.domain.bet_state;

public class GetBetStateUseCase {

    private BetStateAPI _betStateAPI;

    public GetBetStateUseCase(BetStateAPI betStateAPI){
        _betStateAPI = betStateAPI;
    }

    public BetStateModel getCurrentBetState(){
        return _betStateAPI.getCachedCurrentBetState();
    }

    public BetStateModel getLastWinBetState(){
        return _betStateAPI.getCachedLastWinBetState();
    }

    public void refreshBetStates(BetStateCallbacks callbacks){
        _betStateAPI.getBetState(callbacks);
    }

    public String getBetStateWinner(){
        BetStateModel betState = _betStateAPI.getCachedLastWinBetState();
        if(betState != null){
            String status = betState.getStatus();
            if(status.equals("1")){
                return betState.getP1Name();
            } else if(status.equals("2")){
                return betState.getP2Name();
            }
            return BetStateModel.BET_STATE_WINNERS.None.toString();
        }
        return null;
    }

    public String getBetStateStatus(){
        BetStateModel betState = _betStateAPI.getCachedCurrentBetState();
        if(betState != null){
            String status = betState.getStatus();
            if(BetStateModel.BET_STATE_STATUS.open.toString().equals(status)){
                return BetStateModel.BET_STATE_STATUS.open.toString();
            } else if(BetStateModel.BET_STATE_STATUS.locked.toString().equals(status)){
                return BetStateModel.BET_STATE_STATUS.locked.toString();
            }
            return BetStateModel.BET_STATE_STATUS.unknown.toString();
        }
        return null;
    }
}
