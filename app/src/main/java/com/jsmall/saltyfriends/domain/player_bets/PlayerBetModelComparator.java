package com.jsmall.saltyfriends.domain.player_bets;

import java.util.Comparator;

public class PlayerBetModelComparator implements Comparator<PlayerBetModel> {
    public int compare(PlayerBetModel left, PlayerBetModel right){
        if(left.getBet().getTotalWinnings() < right.getBet().getTotalWinnings()){
            return 1;
        } else if(left.getBet().getTotalWinnings() > right.getBet().getTotalWinnings()){
            return -1;
        } else{
            return 0;
        }
    }
}