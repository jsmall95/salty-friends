package com.jsmall.saltyfriends.domain.stats;

import java.util.Comparator;

public class PlayerStatsModelComparator implements Comparator<PlayerStatsModel> {
    public int compare(PlayerStatsModel left, PlayerStatsModel right){
        if(left.getPlayer().getTotalCredits() < right.getPlayer().getTotalCredits()){
            return 1;
        } else if(left.getPlayer().getTotalCredits() > right.getPlayer().getTotalCredits()){
            return -1;
        } else{
            return 0;
        }
    }
}
