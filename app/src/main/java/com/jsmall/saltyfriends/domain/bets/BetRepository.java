package com.jsmall.saltyfriends.domain.bets;

import java.util.ArrayList;

public interface BetRepository {
    ArrayList<BetModel> getPlayerBets(long playerId);
    BetModel getPlayerEventBet(long playerId, long eventId);
    BetModel insertPlayerBet(long playerId, long eventId, String betValue, String betColor, int betAmount, int totalWinnings);
    BetModel updatePlayerBet(BetModel bet);
    boolean deletePlayerBet(long rowId);
    boolean deletePlayerBets(long playerId);
}
