package com.jsmall.saltyfriends.domain.bets;

import android.os.Parcel;
import android.os.Parcelable;

public class BetModel implements Parcelable {
    private long _id;
    private long _playerId;
    private long _eventId;
    private String _betValue;
    private String _betColor;
    private int _betAmount;
    private int _totalWinnings;
    private boolean _active;

    public BetModel(long id, long playerId, long eventId, String betValue, String betColor, int betAmount, int totalWinnings, boolean active){
        _id = id;
        _playerId = playerId;
        _eventId = eventId;
        _betValue = betValue;
        _betColor = betColor;
        _betAmount = betAmount;
        _totalWinnings = totalWinnings;
        _active = active;
    }

    public enum BET_COLORS{
        Red(0xFFFF0000),
        Blue(0xFF349EFF),
        White(0xFFFFFFFF);

        private int colorCode;

        BET_COLORS(int colorCode){
            this.colorCode = colorCode;
        }

        public int getColorCode(){
            return colorCode;
        }
    }

    public long getId(){ return _id; }

    public long getPlayerId(){
        return _playerId;
    }

    public long getEventId() { return _eventId; }

    public String getBetValue() { return _betValue; }

    public void changeBetValue(String betValue) {
        _betValue = betValue;
    }

    public int getBetAmount() { return _betAmount; }
    public void setBetAmount(int betAmount) { _betAmount = betAmount; }

    public int getTotalWinnings() { return _totalWinnings; }
    public void setTotalWinnings(int totalWinnings)
    {
        _totalWinnings = totalWinnings;
        _active = false;
    }

    public boolean getActive() { return _active; }

    public int getBetColor(){
        if(BET_COLORS.Red.toString().equals(_betColor)){
            return BET_COLORS.Red.getColorCode();
        }
        else if(BET_COLORS.Blue.toString().equals(_betColor)){
            return BET_COLORS.Blue.getColorCode();
        }
        else {
            return BET_COLORS.White.getColorCode();
        }
    }
    public String getBetColorString(){
        return _betColor;
    }
    public void setBetColor(String betColor){
        _betColor = betColor;
    }

    protected BetModel(Parcel in) {
        _id = in.readLong();
        _playerId = in.readLong();
        _eventId = in.readLong();
        _betValue = in.readString();
        _betAmount = in.readInt();
        _betColor = in.readString();
        _totalWinnings = in.readInt();
        _active = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_id);
        dest.writeLong(_playerId);
        dest.writeLong(_eventId);
        dest.writeString(_betValue);
        dest.writeInt(_betAmount);
        dest.writeString(_betColor);
        dest.writeInt(_totalWinnings);
        dest.writeByte((byte) (_active ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BetModel> CREATOR = new Parcelable.Creator<BetModel>() {
        @Override
        public BetModel createFromParcel(Parcel in) {
            return new BetModel(in);
        }

        @Override
        public BetModel[] newArray(int size) {
            return new BetModel[size];
        }
    };
}