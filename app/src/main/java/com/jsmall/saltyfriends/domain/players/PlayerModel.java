package com.jsmall.saltyfriends.domain.players;

import android.os.Parcel;
import android.os.Parcelable;

public class PlayerModel implements Parcelable {
    private long _id;
    private int _imageResource;
    private String _playerName;
    private int _totalCredits;

    public PlayerModel(long id, int imageResource, String playerName, int totalCredits){
        _id = id;
        _imageResource = imageResource;
        _playerName = playerName;
        _totalCredits = totalCredits;
    }

    public long getId(){ return _id; }

    public int getImageResource(){
        return _imageResource;
    }

    public String getPlayerName(){
        return _playerName;
    }
    public void setPlayerName(String playerName){ _playerName = playerName; }

    public int getTotalCredits() { return _totalCredits; }
    public void setTotalCredits(int totalCredits) { _totalCredits = totalCredits; }
    public void addTotalCredits(int newCredits) { _totalCredits += newCredits; }

    protected PlayerModel(Parcel in) {
        _id = in.readLong();
        _imageResource = in.readInt();
        _playerName = in.readString();
        _totalCredits = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_id);
        dest.writeInt(_imageResource);
        dest.writeString(_playerName);
        dest.writeInt(_totalCredits);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PlayerModel> CREATOR = new Parcelable.Creator<PlayerModel>() {
        @Override
        public PlayerModel createFromParcel(Parcel in) {
            return new PlayerModel(in);
        }

        @Override
        public PlayerModel[] newArray(int size) {
            return new PlayerModel[size];
        }
    };
}