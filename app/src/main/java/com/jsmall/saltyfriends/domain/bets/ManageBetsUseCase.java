package com.jsmall.saltyfriends.domain.bets;

import java.util.ArrayList;

public class ManageBetsUseCase {
    private BetRepository _repo;

    public ManageBetsUseCase(BetRepository betRepository) { _repo = betRepository; }

    public ArrayList<BetModel> getPlayerBets(long playerId){
        return _repo.getPlayerBets(playerId);
    }

    public BetModel getPlayerEventBet(long playerId, long eventId){
        return _repo.getPlayerEventBet(playerId, eventId);
    }

    public BetModel addPlayerBet(long playerId, long eventId, String betValue, String betColor, int betAmount, int totalWinnings){
        return _repo.insertPlayerBet(playerId, eventId, betValue, betColor, betAmount, totalWinnings);
    }

    public BetModel updateBet(BetModel bet){
        if(bet != null){
            return _repo.updatePlayerBet(bet);
        }
        return null;
    }

    public boolean deletePlayerBet(BetModel bet){
        if(bet != null){
            return _repo.deletePlayerBet(bet.getId());
        }
        return false;
    }

    public boolean deletePlayerBets(long playerId){
        return _repo.deletePlayerBet(playerId);
    }
}
