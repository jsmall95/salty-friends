package com.jsmall.saltyfriends.domain.bet_state;

import java.time.Instant;
import java.util.Date;

public class BetStateModel {
    public enum BET_STATE_WINNERS{
        None,
        Red,
        Blue
    }

    public enum BET_STATE_STATUS{
        unknown,
        open,
        locked
    }

    private String p1name;
    private String p2name;
    private String status;

    private long _dateTime;

    public String getP1Name(){
        return p1name;
    }

    public String getP2Name(){
        return p2name;
    }

    public String getStatus(){
        return status;
    }

    public long getTime(){ return _dateTime; }
    public void setTime(){ _dateTime = Date.from(Instant.now()).getTime(); }
}
