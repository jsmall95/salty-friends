package com.jsmall.saltyfriends.domain.bet_state;

public interface BetStateAPI {

    void getBetState(BetStateCallbacks callbacks);

    BetStateModel getCachedCurrentBetState();

    BetStateModel getCachedLastWinBetState();
}
