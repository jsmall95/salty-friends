package com.jsmall.saltyfriends.domain.player_bets;

import android.os.Parcel;
import android.os.Parcelable;

import com.jsmall.saltyfriends.domain.bets.BetModel;
import com.jsmall.saltyfriends.domain.players.PlayerModel;

public class PlayerBetModel implements Parcelable {
    private PlayerModel _player;
    private BetModel _bet;

    public PlayerBetModel(PlayerModel player, BetModel bet){
        _player = player;
        _bet = bet;
    }

    public PlayerModel getPlayer(){ return _player; }

    public BetModel getBet() { return _bet; }


    protected PlayerBetModel(Parcel in) {
        _player = (PlayerModel) in.readValue(PlayerModel.class.getClassLoader());
        _bet = (BetModel) in.readValue(BetModel.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(_player);
        dest.writeValue(_bet);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PlayerBetModel> CREATOR = new Parcelable.Creator<PlayerBetModel>() {
        @Override
        public PlayerBetModel createFromParcel(Parcel in) {
            return new PlayerBetModel(in);
        }

        @Override
        public PlayerBetModel[] newArray(int size) {
            return new PlayerBetModel[size];
        }
    };
}
