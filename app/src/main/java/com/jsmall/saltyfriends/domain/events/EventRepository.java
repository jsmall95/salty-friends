package com.jsmall.saltyfriends.domain.events;

public interface EventRepository {

    EventModel getActiveEvent();

    EventModel getLastEvent();

    EventModel insertEvent(String redPlayer, String bluePlayer);

    EventModel updateEvent(EventModel event);

}
