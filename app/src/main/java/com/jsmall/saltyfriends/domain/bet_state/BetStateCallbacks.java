package com.jsmall.saltyfriends.domain.bet_state;

public interface BetStateCallbacks{
    void updatedLastWinBetStateReady();
    void updatedCurrentBetStateReady();
}