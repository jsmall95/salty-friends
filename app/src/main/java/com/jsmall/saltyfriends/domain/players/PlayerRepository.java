package com.jsmall.saltyfriends.domain.players;

import java.util.ArrayList;

public interface PlayerRepository {

    ArrayList<PlayerModel> getPlayers();

    PlayerModel getPlayer(long playerId);

    PlayerModel insertPlayer(String playerName, int imageResource, int totalCredits);

    PlayerModel updatePlayer(PlayerModel player);

    boolean deletePlayer(long rowId);
}
