package com.jsmall.saltyfriends.domain.stats;

import android.os.Parcel;
import android.os.Parcelable;

import com.jsmall.saltyfriends.domain.players.PlayerModel;

public class PlayerStatsModel implements Parcelable {

    private PlayerModel _player;
    private int _totalWins;
    private int _totalLosses;

    public PlayerStatsModel(PlayerModel player, int totalWins, int totalLosses){
        _player = player;
        _totalWins = totalWins;
        _totalLosses = totalLosses;
    }

    protected PlayerStatsModel(Parcel in) {
        _player = in.readParcelable(PlayerModel.class.getClassLoader());
        _totalWins = in.readInt();
        _totalLosses = in.readInt();
    }

    public PlayerModel getPlayer(){ return _player; }
    public int getTotalWins(){ return _totalWins; }
    public int getTotalLosses(){ return _totalLosses; }

    public static final Creator<PlayerStatsModel> CREATOR = new Creator<PlayerStatsModel>() {
        @Override
        public PlayerStatsModel createFromParcel(Parcel in) {
            return new PlayerStatsModel(in);
        }

        @Override
        public PlayerStatsModel[] newArray(int size) {
            return new PlayerStatsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(_player, flags);
        dest.writeInt(_totalWins);
        dest.writeInt(_totalLosses);
    }
}
