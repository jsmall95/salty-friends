package com.jsmall.saltyfriends.domain.events;

import com.jsmall.saltyfriends.domain.bets.BetModel;
import com.jsmall.saltyfriends.domain.bets.BetRepository;
import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.domain.players.PlayerModel;
import com.jsmall.saltyfriends.domain.players.PlayerRepository;

import java.util.ArrayList;

public class ManageEventsUseCase {
    private EventRepository _eventRepo;
    private PlayerRepository _playerRepo;
    private BetRepository _betRepo;

    public ManageEventsUseCase(EventRepository eventRepository, PlayerRepository playerRepository, BetRepository betRepository){
        _eventRepo = eventRepository;
        _playerRepo = playerRepository;
        _betRepo = betRepository;
    }

    public EventModel getActiveEvent(){
        EventModel activeEvent = _eventRepo.getActiveEvent();
        if(activeEvent != null){
            ArrayList<PlayerBetModel> playerBets = new ArrayList<>();
            ArrayList<PlayerModel> players = _playerRepo.getPlayers();

            for(PlayerModel player : players){
                BetModel bet = _betRepo.getPlayerEventBet(player.getId(), activeEvent.getId());
                if(bet == null){
                    bet = _betRepo.insertPlayerBet(player.getId(), activeEvent.getId(), "", BetModel.BET_COLORS.White.toString(), 0, 0);
                }
                playerBets.add(new PlayerBetModel(player, bet));
            }

            activeEvent.setPlayerBets(playerBets);
        }
        else{
            activeEvent = addEvent("Red", "Blue");
            ArrayList<PlayerBetModel> playerBets = new ArrayList<>();
            ArrayList<PlayerModel> players = _playerRepo.getPlayers();

            for (PlayerModel player : players) {
                playerBets.add(new PlayerBetModel(player, _betRepo.insertPlayerBet(player.getId(), activeEvent.getId(), "", BetModel.BET_COLORS.White.toString(), 0, 0)));
            }
            activeEvent.setPlayerBets(playerBets);
        }

        return activeEvent;
    }

    public EventModel getLastEvent(){
        EventModel lastEvent = _eventRepo.getLastEvent();
        if(lastEvent != null){
            ArrayList<PlayerBetModel> playerBets = new ArrayList<>();
            ArrayList<PlayerModel> players = _playerRepo.getPlayers();

            for(PlayerModel player : players){
                BetModel bet = _betRepo.getPlayerEventBet(player.getId(), lastEvent.getId());
                if(bet == null){
                    bet = _betRepo.insertPlayerBet(player.getId(), lastEvent.getId(), "", BetModel.BET_COLORS.White.toString(), 0, 0);
                }
                playerBets.add(new PlayerBetModel(player, bet));
            }

            lastEvent.setPlayerBets(playerBets);
        }

        return lastEvent;
    }

    public EventModel addEvent(String redPlayer, String bluePlayer){
        return _eventRepo.insertEvent(redPlayer, bluePlayer);
    }

    public EventModel updateEvent(EventModel event){
        return _eventRepo.updateEvent(event);
    }

    public int getWinnerColor(EventModel event){
        if(event != null){
            String winner = event.getWinner();
            if(winner != null && !winner.isEmpty()){
                if(winner.equals(event.getRedPlayer())){
                    return BetModel.BET_COLORS.Red.getColorCode();
                } else {
                    return BetModel.BET_COLORS.Blue.getColorCode();
                }
            }
        }
        return BetModel.BET_COLORS.White.getColorCode();
    }

}
