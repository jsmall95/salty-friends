package com.jsmall.saltyfriends.data.sq_lite.players;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jsmall.saltyfriends.domain.players.PlayerModel;
import com.jsmall.saltyfriends.domain.players.PlayerRepository;
import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;

import java.util.ArrayList;

public class SQLitePlayerRepository implements PlayerRepository {

    private final DbHelper _dbHelper;

    public SQLitePlayerRepository(DbHelper dbHelper){
        _dbHelper = dbHelper;
    }

    public ArrayList<PlayerModel> getPlayers(){
        SQLiteDatabase db = _dbHelper.getReadableDatabase();

        String[] projection = {
                PlayerContract.PlayerInfo._ID,
                PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME,
                PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE,
                PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS
        };

        String sortOrder = PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME + " DESC";

        Cursor cursor = db.query(
                PlayerContract.PlayerInfo.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );

        ArrayList<PlayerModel> players = new ArrayList<PlayerModel>();
        while(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo._ID));
            int image_id = cursor.getInt(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE));
            String playerName = cursor.getString(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME));
            int totalCredits = cursor.getInt(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS));

            players.add(new PlayerModel(id, image_id, playerName, totalCredits));
        }

        cursor.close();

        return players;
    }

    public PlayerModel getPlayer(long playerId){
        SQLiteDatabase db = _dbHelper.getReadableDatabase();

        String[] projection = {
                PlayerContract.PlayerInfo._ID,
                PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME,
                PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE,
                PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS
        };

        String selection = PlayerContract.PlayerInfo._ID + " = ?";
        String[] selectionArgs = {Long.toString(playerId)};


        Cursor cursor = db.query(
                PlayerContract.PlayerInfo.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        PlayerModel player = null;
        while(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo._ID));
            int image_id = cursor.getInt(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE));
            String playerName = cursor.getString(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME));
            int totalCredits = cursor.getInt(cursor.getColumnIndexOrThrow(PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS));

            player = new PlayerModel(id, image_id, playerName, totalCredits);
        }

        cursor.close();

        return player;
    }

    public PlayerModel insertPlayer(String playerName, int image_resource, int totalCredits){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME, playerName);
        values.put(PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE, image_resource);
        values.put(PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS, totalCredits);

        long newRowId = db.insert(PlayerContract.PlayerInfo.TABLE_NAME, null, values);
        if(newRowId != -1){
            return new PlayerModel(newRowId, image_resource, playerName, totalCredits);
        }
        return null;
    }

    public PlayerModel updatePlayer(PlayerModel player){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME, player.getPlayerName());
        values.put(PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE, player.getImageResource());
        values.put(PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS, player.getTotalCredits());

        String selection = PlayerContract.PlayerInfo._ID + " = ?";
        String[] selectionArgs = { Long.toString(player.getId())};

        int count = db.update(
                PlayerContract.PlayerInfo.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        if(count > 0){
            return player;
        }
        else{
            return null;
        }
    }

    public boolean deletePlayer(long rowId){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        String selection = PlayerContract.PlayerInfo._ID + " = ?";
        String[] selectionArgs = { Long.toString(rowId)};

        int deletedRows = db.delete(PlayerContract.PlayerInfo.TABLE_NAME, selection, selectionArgs);

        return deletedRows > 0;
    }
}
