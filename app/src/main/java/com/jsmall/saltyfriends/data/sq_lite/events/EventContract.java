package com.jsmall.saltyfriends.data.sq_lite.events;

import android.provider.BaseColumns;

public class EventContract {

    private EventContract(){}

    public static class EventInfo implements BaseColumns {
        public static final String TABLE_NAME = "event_info";
        public static final String COLUMN_NAME_FINALIZED = "finalized";
        public static final String COLUMN_NAME_RED_PLAYER = "red_player";
        public static final String COLUMN_NAME_BLUE_PLAYER = "blue_player";
        public static final String COLUMN_NAME_WINNER = "winner";
        public static final String COLUMN_NAME_ACTIVE = "active";
        public static final String COLUMN_NAME_LAST_ACTIVE = "last_active";
    }

}
