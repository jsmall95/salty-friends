package com.jsmall.saltyfriends.data.sq_lite.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jsmall.saltyfriends.data.sq_lite.bets.BetContract;
import com.jsmall.saltyfriends.data.sq_lite.events.EventContract;
import com.jsmall.saltyfriends.data.sq_lite.players.PlayerContract;

public class DbHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_PLAYER_TABLE = "CREATE TABLE " + PlayerContract.PlayerInfo.TABLE_NAME + " (" +
                    PlayerContract.PlayerInfo._ID + " INTEGER PRIMARY KEY," +
                    PlayerContract.PlayerInfo.COLUMN_NAME_PLAYER_NAME + " TEXT," +
                    PlayerContract.PlayerInfo.COLUMN_NAME_IMAGE_RESOURCE + " INTEGER," +
                    PlayerContract.PlayerInfo.COLUMN_NAME_TOTAL_CREDITS + " INTEGER)";

    private static final String SQL_CREATE_EVENT_TABLE = "CREATE TABLE " + EventContract.EventInfo.TABLE_NAME + " (" +
            EventContract.EventInfo._ID + " INTEGER PRIMARY KEY," +
            EventContract.EventInfo.COLUMN_NAME_FINALIZED + " NUMERIC," +
            EventContract.EventInfo.COLUMN_NAME_RED_PLAYER + " TEXT," +
            EventContract.EventInfo.COLUMN_NAME_BLUE_PLAYER + " TEXT," +
            EventContract.EventInfo.COLUMN_NAME_WINNER + " TEXT," +
            EventContract.EventInfo.COLUMN_NAME_ACTIVE + " NUMERIC," +
            EventContract.EventInfo.COLUMN_NAME_LAST_ACTIVE + " INTEGER)";

    private static final String SQL_CREATE_BET_TABLE = "CREATE TABLE " + BetContract.BetInfo.TABLE_NAME + " (" +
            BetContract.BetInfo._ID + " INTEGER PRIMARY KEY," +
            BetContract.BetInfo.COLUMN_NAME_PLAYER_ID + " INTERGER," +
            BetContract.BetInfo.COLUMN_NAME_EVENT_ID + " INTERGER," +
            BetContract.BetInfo.COLUMN_NAME_BET_VALUE + " TEXT," +
            BetContract.BetInfo.COLUMN_NAME_BET_COLOR + " TEXT," +
            BetContract.BetInfo.COLUMN_NAME_BET_AMOUNT + " INTEGER," +
            BetContract.BetInfo.COLUMN_NAME_TOTAL_WINNINGS + " INTEGER," +
            BetContract.BetInfo.COLUMN_NAME_ACTIVE + " INTEGER," +
            " FOREIGN KEY ("+BetContract.BetInfo.COLUMN_NAME_PLAYER_ID+") REFERENCES "+PlayerContract.PlayerInfo.TABLE_NAME+"("+PlayerContract.PlayerInfo._ID+")," +
            " FOREIGN KEY ("+BetContract.BetInfo.COLUMN_NAME_EVENT_ID+") REFERENCES "+EventContract.EventInfo.TABLE_NAME+"("+EventContract.EventInfo._ID+"));";


    private static final String SQL_DELETE_PLAYER_TABLE =
            "DROP TABLE IF EXISTS " + PlayerContract.PlayerInfo.TABLE_NAME;
    private static final String SQL_DELETE_EVENT_TABLE =
            "DROP TABLE IF EXISTS " + EventContract.EventInfo.TABLE_NAME;
    private static final String SQL_DELETE_BET_TABLE =
            "DROP TABLE IF EXISTS " + BetContract.BetInfo.TABLE_NAME;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "salty_friends.db";


    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PLAYER_TABLE);
        db.execSQL(SQL_CREATE_EVENT_TABLE);
        db.execSQL(SQL_CREATE_BET_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_BET_TABLE);
        db.execSQL(SQL_DELETE_EVENT_TABLE);
        db.execSQL(SQL_DELETE_PLAYER_TABLE);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
}
