package com.jsmall.saltyfriends.data.io_socket;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URI;
import java.net.URISyntaxException;

public class SocketClientModel {

    private Socket _socket;

    public SocketClientModel(String url){
        try {
            URI uri = new URI(url);
            _socket = IO.socket(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket(){
        return _socket;
    }
}
