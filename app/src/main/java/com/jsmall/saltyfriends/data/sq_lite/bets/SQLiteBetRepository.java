package com.jsmall.saltyfriends.data.sq_lite.bets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jsmall.saltyfriends.domain.bets.BetModel;
import com.jsmall.saltyfriends.domain.bets.BetRepository;
import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;

import java.util.ArrayList;

public class SQLiteBetRepository implements BetRepository {

    private final DbHelper _dbHelper;

    private final String[] _projection = {
            BetContract.BetInfo._ID,
            BetContract.BetInfo.COLUMN_NAME_PLAYER_ID,
            BetContract.BetInfo.COLUMN_NAME_EVENT_ID,
            BetContract.BetInfo.COLUMN_NAME_BET_VALUE,
            BetContract.BetInfo.COLUMN_NAME_BET_COLOR,
            BetContract.BetInfo.COLUMN_NAME_BET_AMOUNT,
            BetContract.BetInfo.COLUMN_NAME_TOTAL_WINNINGS,
            BetContract.BetInfo.COLUMN_NAME_ACTIVE
    };

    public SQLiteBetRepository(DbHelper dbHelper){
        _dbHelper = dbHelper;
    }

    public ArrayList<BetModel> getPlayerBets(long playerId){
        SQLiteDatabase db = _dbHelper.getReadableDatabase();

        String selection = BetContract.BetInfo.COLUMN_NAME_PLAYER_ID + " = ?";
        String[] selectionArgs = { Long.toString(playerId)};

        Cursor cursor = db.query(
                BetContract.BetInfo.TABLE_NAME,
                _projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        ArrayList<BetModel> playerBets = new ArrayList<BetModel>();
        while(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(BetContract.BetInfo._ID));
            String betValue = cursor.getString(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_BET_VALUE));
            String betColor = cursor.getString(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_BET_COLOR));
            int eventId = cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_EVENT_ID));
            int betAmount = cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_BET_AMOUNT));
            int totalWinnings = cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_TOTAL_WINNINGS));
            boolean active = (cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_ACTIVE)) > 0);

            playerBets.add(new BetModel(id, playerId, eventId, betValue, betColor, betAmount, totalWinnings, active));
        }

        cursor.close();

        return playerBets;
    }

    public BetModel getPlayerEventBet(long playerId, long eventId){
        SQLiteDatabase db = _dbHelper.getReadableDatabase();

        String sortOrder = BetContract.BetInfo.COLUMN_NAME_PLAYER_ID + " DESC";
        String selection = BetContract.BetInfo.COLUMN_NAME_PLAYER_ID + " = ?" + " AND " + BetContract.BetInfo.COLUMN_NAME_EVENT_ID + " = ?";
        String[] selectionArgs = { Long.toString(playerId), Long.toString(eventId)};

        Cursor cursor = db.query(
                BetContract.BetInfo.TABLE_NAME,
                _projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        BetModel bet = null;
        while(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(BetContract.BetInfo._ID));
            String betValue = cursor.getString(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_BET_VALUE));
            String betColor = cursor.getString(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_BET_COLOR));
            int betAmount = cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_BET_AMOUNT));
            int totalWinnings = cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_TOTAL_WINNINGS));
            boolean active = (cursor.getInt(cursor.getColumnIndexOrThrow(BetContract.BetInfo.COLUMN_NAME_ACTIVE)) > 0);

            bet = new BetModel(id, playerId, eventId, betValue, betColor, betAmount, totalWinnings, active);
        }

        cursor.close();

        return bet;
    }

    public BetModel insertPlayerBet(long playerId, long eventId, String betValue, String betColor, int betAmount, int totalWinnings){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BetContract.BetInfo.COLUMN_NAME_PLAYER_ID, playerId);
        values.put(BetContract.BetInfo.COLUMN_NAME_EVENT_ID, eventId);
        values.put(BetContract.BetInfo.COLUMN_NAME_BET_VALUE, betValue);
        values.put(BetContract.BetInfo.COLUMN_NAME_BET_COLOR, betColor);
        values.put(BetContract.BetInfo.COLUMN_NAME_BET_AMOUNT, betAmount);
        values.put(BetContract.BetInfo.COLUMN_NAME_TOTAL_WINNINGS, totalWinnings);
        values.put(BetContract.BetInfo.COLUMN_NAME_ACTIVE, 1);

        long newRowId = db.insert(BetContract.BetInfo.TABLE_NAME, null, values);
        if(newRowId != -1){
            return new BetModel(newRowId, playerId, eventId, betValue, betColor, betAmount, totalWinnings, true);
        }
        return null;
    }

    public BetModel updatePlayerBet(BetModel bet){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BetContract.BetInfo.COLUMN_NAME_BET_VALUE, bet.getBetValue());
        values.put(BetContract.BetInfo.COLUMN_NAME_BET_COLOR, bet.getBetColorString());
        values.put(BetContract.BetInfo.COLUMN_NAME_BET_AMOUNT, bet.getBetAmount());
        values.put(BetContract.BetInfo.COLUMN_NAME_TOTAL_WINNINGS, bet.getTotalWinnings());
        values.put(BetContract.BetInfo.COLUMN_NAME_ACTIVE, bet.getActive());

        String selection = BetContract.BetInfo._ID + " = ?";
        String[] selectionArgs = { Long.toString(bet.getId())};

        int count = db.update(
                BetContract.BetInfo.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        if(count > 0){
            return bet;
        }
        else{
            return null;
        }
    }

    public boolean deletePlayerBet(long rowId){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        String selection = BetContract.BetInfo._ID + " = ?";
        String[] selectionArgs = { Long.toString(rowId)};

        int deletedRows = db.delete(BetContract.BetInfo.TABLE_NAME, selection, selectionArgs);

        return deletedRows > 0;
    }

    public boolean deletePlayerBets(long playerId){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        String selection = BetContract.BetInfo.COLUMN_NAME_PLAYER_ID + " = ?";
        String[] selectionArgs = { Long.toString(playerId)};

        int deletedRows = db.delete(BetContract.BetInfo.TABLE_NAME, selection, selectionArgs);

        return deletedRows > 0;
    }

    public void close(){
        _dbHelper.close();
    }
}
