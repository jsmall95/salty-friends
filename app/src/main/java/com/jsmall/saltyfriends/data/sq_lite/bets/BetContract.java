package com.jsmall.saltyfriends.data.sq_lite.bets;

import android.provider.BaseColumns;

public final class BetContract {
    private BetContract(){}

    public static class BetInfo implements BaseColumns {
        public static final String TABLE_NAME = "bet_info";
        public static final String COLUMN_NAME_PLAYER_ID = "player_id";
        public static final String COLUMN_NAME_EVENT_ID = "event_id";
        public static final String COLUMN_NAME_BET_VALUE = "bet_value";
        public static final String COLUMN_NAME_BET_COLOR = "bet_color";
        public static final String COLUMN_NAME_BET_AMOUNT = "bet_amount";
        public static final String COLUMN_NAME_TOTAL_WINNINGS = "total_winnings";
        public static final String COLUMN_NAME_ACTIVE = "active";
    }

}
