package com.jsmall.saltyfriends.data.sq_lite.events;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jsmall.saltyfriends.domain.events.EventModel;
import com.jsmall.saltyfriends.domain.events.EventRepository;
import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;

import java.time.Instant;
import java.util.Date;

public class SQLiteEventRepository implements EventRepository {

    private final DbHelper _dbHelper;

    public SQLiteEventRepository(DbHelper dbHelper){
        _dbHelper = dbHelper;
    }

    private final String[] _projection = {
            EventContract.EventInfo._ID,
            EventContract.EventInfo.COLUMN_NAME_FINALIZED,
            EventContract.EventInfo.COLUMN_NAME_RED_PLAYER,
            EventContract.EventInfo.COLUMN_NAME_BLUE_PLAYER,
            EventContract.EventInfo.COLUMN_NAME_WINNER,
            EventContract.EventInfo.COLUMN_NAME_ACTIVE,
            EventContract.EventInfo.COLUMN_NAME_LAST_ACTIVE
    };

    public EventModel getActiveEvent(){
        SQLiteDatabase db = _dbHelper.getReadableDatabase();

        String sortOrder = EventContract.EventInfo._ID + " DESC";
        String selection = EventContract.EventInfo.COLUMN_NAME_ACTIVE + " = ?";
        String[] selectionArgs = { "1" };


        Cursor cursor = db.query(
                EventContract.EventInfo.TABLE_NAME,
                _projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        EventModel event = null;
        while(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(EventContract.EventInfo._ID));
            boolean finalized = (cursor.getInt(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_FINALIZED)) > 0);
            String redPlayer = cursor.getString(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_RED_PLAYER));
            String bluePlayer = cursor.getString(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_BLUE_PLAYER));
            String winner = cursor.getString(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_WINNER));
            boolean active = (cursor.getInt(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_ACTIVE)) > 0);

            event = new EventModel(id, finalized, redPlayer, bluePlayer, winner, active);
        }

        cursor.close();

        return event;
    }

    public EventModel getLastEvent(){
        SQLiteDatabase db = _dbHelper.getReadableDatabase();

        String sortOrder = EventContract.EventInfo.COLUMN_NAME_LAST_ACTIVE + " DESC";
        String selection = EventContract.EventInfo.COLUMN_NAME_ACTIVE + " = ?";
        String[] selectionArgs = { "0" };


        Cursor cursor = db.query(
                EventContract.EventInfo.TABLE_NAME,
                _projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        EventModel event = null;
        while(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(EventContract.EventInfo._ID));
            boolean finalized = (cursor.getInt(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_FINALIZED)) > 0);
            String redPlayer = cursor.getString(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_RED_PLAYER));
            String bluePlayer = cursor.getString(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_BLUE_PLAYER));
            String winner = cursor.getString(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_WINNER));
            boolean active = (cursor.getInt(cursor.getColumnIndexOrThrow(EventContract.EventInfo.COLUMN_NAME_ACTIVE)) > 0);

            event = new EventModel(id, finalized, redPlayer, bluePlayer, winner, active);
        }

        cursor.close();

        return event;
    }

    public EventModel insertEvent(String redPlayer, String bluePlayer){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EventContract.EventInfo.COLUMN_NAME_FINALIZED, 0);
        values.put(EventContract.EventInfo.COLUMN_NAME_RED_PLAYER, redPlayer);
        values.put(EventContract.EventInfo.COLUMN_NAME_BLUE_PLAYER, bluePlayer);
        values.put(EventContract.EventInfo.COLUMN_NAME_WINNER, "");
        values.put(EventContract.EventInfo.COLUMN_NAME_ACTIVE, 1);

        long newRowId = db.insert(EventContract.EventInfo.TABLE_NAME, null, values);
        if(newRowId != -1){
            return new EventModel(newRowId, false, redPlayer, bluePlayer, "", true);
        }
        return null;
    }

    public EventModel updateEvent(EventModel event){
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EventContract.EventInfo.COLUMN_NAME_FINALIZED, event.isFinalized() ? 1 : 0);
        values.put(EventContract.EventInfo.COLUMN_NAME_RED_PLAYER, event.getRedPlayer());
        values.put(EventContract.EventInfo.COLUMN_NAME_BLUE_PLAYER, event.getBluePlayer());
        values.put(EventContract.EventInfo.COLUMN_NAME_WINNER, event.getWinner());
        values.put(EventContract.EventInfo.COLUMN_NAME_ACTIVE, event.isActive() ? 1 : 0);
        values.put(EventContract.EventInfo.COLUMN_NAME_LAST_ACTIVE, Date.from(Instant.now()).getTime());

        String selection = EventContract.EventInfo._ID + " = ?";
        String[] selectionArgs = { Long.toString(event.getId())};

        int count = db.update(
                EventContract.EventInfo.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        if(count > 0){
            return event;
        }
        else{
            return null;
        }
    }
}
