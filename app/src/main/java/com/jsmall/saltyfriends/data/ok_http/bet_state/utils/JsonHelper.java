package com.jsmall.saltyfriends.data.ok_http.bet_state.utils;

import com.google.gson.Gson;

public class JsonHelper {

    public static <T> T convertJsonToObject(String jsonString, Class<T> objectClass){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, objectClass);
    }

    public static <T> String convertObjectToJson(T object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}
