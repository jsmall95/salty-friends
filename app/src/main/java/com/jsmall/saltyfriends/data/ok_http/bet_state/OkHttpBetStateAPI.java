package com.jsmall.saltyfriends.data.ok_http.bet_state;

import com.jsmall.saltyfriends.domain.bet_state.BetStateAPI;
import com.jsmall.saltyfriends.domain.bet_state.BetStateCallbacks;
import com.jsmall.saltyfriends.domain.bet_state.BetStateModel;
import com.jsmall.saltyfriends.data.ok_http.bet_state.utils.JsonHelper;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpBetStateAPI implements BetStateAPI {

    private static final String _url = "https://www.saltybet.com";
    private static final String _getStateAPI = "/state.json?t=";

    private BetStateModel _cachedCurrentBetState;
    private BetStateModel _cachedLastWinBetState;

    public void getBetState(final BetStateCallbacks callbacks){
        OkHttpClient client = new OkHttpClient();
        String url = getStateAPIUrl();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                _cachedCurrentBetState = null;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                BetStateModel betState = JsonHelper.convertJsonToObject(response.body().string(), BetStateModel.class);
                betState.setTime();
                if(betState.getStatus().equals("1") || betState.getStatus().equals("2")){
                    _cachedLastWinBetState = betState;
                    callbacks.updatedLastWinBetStateReady();
                }
                else{
                    _cachedCurrentBetState = betState;
                    callbacks.updatedCurrentBetStateReady();
                }
            }
        });
    }

    public BetStateModel getCachedLastWinBetState(){
        return _cachedLastWinBetState;
    }

    public BetStateModel getCachedCurrentBetState(){
        return _cachedCurrentBetState;
    }

    private String getStateAPIUrl(){
        return _url + _getStateAPI + Calendar.getInstance().getTimeInMillis();
    }
}
