package com.jsmall.saltyfriends.data.sq_lite.players;

import android.provider.BaseColumns;

public final class PlayerContract {
    private PlayerContract(){}

    public static class PlayerInfo implements BaseColumns {
        public static final String TABLE_NAME = "player_info";
        public static final String COLUMN_NAME_PLAYER_NAME = "name";
        public static final String COLUMN_NAME_IMAGE_RESOURCE = "avatar_int";
        public static final String COLUMN_NAME_TOTAL_CREDITS = "total_credits";
    }

}
