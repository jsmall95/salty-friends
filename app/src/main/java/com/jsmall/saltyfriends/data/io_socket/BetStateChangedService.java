package com.jsmall.saltyfriends.data.io_socket;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.injection.component.DataAccessComponent;
import com.jsmall.saltyfriends.domain.bet_state.BetStateCallbacks;
import com.jsmall.saltyfriends.domain.bet_state.GetBetStateUseCase;

import javax.inject.Inject;

import androidx.annotation.Nullable;

public class BetStateChangedService extends Service implements BetStateCallbacks {

    private final static String BET_STATE_CHANGED_URL = "https://www.saltybet.com:2096";

    private boolean _running;
    private SocketClientModel _socketClient;

    @Inject
    public GetBetStateUseCase getBetStateUseCase;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        DataAccessComponent.initialize().inject(this);
        _running = false;
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!_running){
            _socketClient = new SocketClientModel(BET_STATE_CHANGED_URL);
            _socketClient.getSocket().on("message", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    updateBetStates();
                }
            });
            _socketClient.getSocket().connect();
            if(_socketClient.getSocket().connected()){
                Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
                _running = true;
            }
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        if(_socketClient.getSocket() != null){
            _socketClient.getSocket().close();
        }
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
    }

    private void updateBetStates(){
        getBetStateUseCase.refreshBetStates(this);
    }

    @Override
    public void updatedLastWinBetStateReady() {
        Intent intent = new Intent(getString(R.string.update_last_win_bet_state));
        sendBroadcast(intent);
    }

    @Override
    public void updatedCurrentBetStateReady() {
        Intent intent = new Intent(getString(R.string.update_current_bet_state));
        sendBroadcast(intent);
    }
}
