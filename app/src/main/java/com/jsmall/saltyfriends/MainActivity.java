package com.jsmall.saltyfriends;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.jsmall.saltyfriends.data.io_socket.BetStateChangedService;
import com.jsmall.saltyfriends.ui.bets.EventWinnerSummaryFragment;
import com.jsmall.saltyfriends.ui.bets.PlaceBetsFragment;
import com.jsmall.saltyfriends.ui.players.ViewPlayersFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jsmall.saltyfriends.ui.stats.ViewStatsFragment;
import com.jsmall.saltyfriends.injection.component.AppComponent;
import com.jsmall.saltyfriends.util.MediaPlayerService;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    public static final String ACTIVE_EVENT = "com.example.saltyfriends.ACTIVE_EVENT";

    public static final String BETS_VIEW = "bets_view";
    public static final String LAST_RESULT_VIEW = "last_result_view";
    public static final String STATS_VIEW = "stats_view";
    public static final String PLAYERS_VIEW = "players_view";

    private BottomNavigationView _bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnimationDrawable loadingAnimation = (AnimationDrawable) findViewById(R.id.salt_loading_animation).getBackground();
        loadingAnimation.start();

        _bottomNavigationView = findViewById(R.id.bottom_navigation);
        _bottomNavigationView.setOnNavigationItemSelectedListener(this);
        _bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        _bottomNavigationView.setSelectedItemId(R.id.bets_view);

    }

    @Override
    protected void onPause(){
        super.onPause();
        stopService(new Intent(this, BetStateChangedService.class));
        stopService(new Intent(this, MediaPlayerService.class));

    }

    @Override
    protected void onResume(){
        super.onResume();
        startService(new Intent(this, BetStateChangedService.class));
        startService(new Intent(this, MediaPlayerService.class));
    }

    @Override
    protected void onStop(){
        super.onStop();
        stopService(new Intent(this, BetStateChangedService.class));
        stopService(new Intent(this, MediaPlayerService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppComponent.get().getDbHelper().close();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        String fragmentName = "";
        switch(menuItem.getItemId()){
            case R.id.bets_view:
                fragment = new PlaceBetsFragment();
                fragmentName = BETS_VIEW;
                break;
            case R.id.last_result_view:
                fragment = new EventWinnerSummaryFragment();
                fragmentName = LAST_RESULT_VIEW;
                break;
            case R.id.results_view:
                fragment = new ViewStatsFragment();
                fragmentName = STATS_VIEW;
                break;
            case R.id.players_view:
                fragment = new ViewPlayersFragment();
                fragmentName = PLAYERS_VIEW;
                break;
        }

        return loadFragment(fragment, fragmentName, false);
    }

    public void navigateFromMenu(int menuId){
        _bottomNavigationView.setSelectedItemId(menuId);
    }

    @Override
    public void onBackPressed(){
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() > 1){
            fm.popBackStackImmediate();
        } else if(fm.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public boolean loadFragment(Fragment fragment, String fragmentName, boolean removeCurrent){
        if(fragment != null && !fragment.isVisible()){
            FragmentManager fm = getSupportFragmentManager();
            if(removeCurrent){
                fm.popBackStack();
                fm.beginTransaction().setCustomAnimations(R.animator.fade_in, R.animator.fade_out).replace(R.id.fragment_container, fragment, fragmentName).addToBackStack(fragmentName).commit();
            }
            else{
                fm.beginTransaction().setCustomAnimations(R.animator.fade_in, R.animator.fade_out).replace(R.id.fragment_container, fragment, fragmentName).addToBackStack(fragmentName).commit();
            }

            enableLoadingPanel();

            return true;
        }

        return false;
    }

    public void enableLoadingPanel(){
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        findViewById(R.id.fragment_container).setVisibility(View.GONE);
    }

    public void disableLoadingPanel(){
        findViewById(R.id.fragment_container).setVisibility(View.VISIBLE);
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }
}
