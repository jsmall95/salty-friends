package com.jsmall.saltyfriends.injection.component;

import android.content.Context;

import com.jsmall.saltyfriends.domain.bet_state.BetStateAPI;
import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.bets.BetRepository;
import com.jsmall.saltyfriends.domain.events.EventRepository;
import com.jsmall.saltyfriends.domain.players.PlayerRepository;
import com.jsmall.saltyfriends.injection.module.AppModule;
import com.jsmall.saltyfriends.injection.module.BetRepositoryModule;
import com.jsmall.saltyfriends.injection.module.BetStateServiceModule;
import com.jsmall.saltyfriends.injection.module.DbHelperModule;
import com.jsmall.saltyfriends.injection.module.EventRepositoryModule;
import com.jsmall.saltyfriends.injection.module.PlayerRepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class,
        BetStateServiceModule.class,
        DbHelperModule.class,
        PlayerRepositoryModule.class,
        EventRepositoryModule.class,
        BetRepositoryModule.class})
public abstract class AppComponent {
    private static AppComponent _instance;

    public static AppComponent get(){
        return AppComponent._instance;
    }

    public static void initialize(
            AppModule appModule,
            BetStateServiceModule betStateServiceModule,
            DbHelperModule dbHelperModule,
            PlayerRepositoryModule playerRepositoryModule,
            EventRepositoryModule eventRepositoryModule,
            BetRepositoryModule betRepositoryModule
            ){
        if(AppComponent.get() != null){
            throw new RuntimeException("AppComponent already initialized");
        }
        AppComponent._instance = DaggerAppComponent
                .builder()
                .appModule(appModule)
                .betStateServiceModule(betStateServiceModule)
                .dbHelperModule(dbHelperModule)
                .playerRepositoryModule(playerRepositoryModule)
                .eventRepositoryModule(eventRepositoryModule)
                .betRepositoryModule(betRepositoryModule)
                .build();
    }

    abstract public Context getAppContext();

    abstract public BetStateAPI getBetStateService();

    abstract public DbHelper getDbHelper();

    abstract public PlayerRepository getPlayerRepository();

    abstract public EventRepository getEventRepository();

    abstract public BetRepository getBetRepository();

}
