package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.bets.BetRepository;
import com.jsmall.saltyfriends.data.sq_lite.bets.SQLiteBetRepository;

public class SQLiteBetRepositoryModule extends BetRepositoryModule {

    @Override
    public BetRepository provideBetRepository(DbHelper dbHelper) {
        return provideSQLiteBetRepository(dbHelper);
    }

    public BetRepository provideSQLiteBetRepository(DbHelper dbHelper){
        return new SQLiteBetRepository(dbHelper);
    }
}
