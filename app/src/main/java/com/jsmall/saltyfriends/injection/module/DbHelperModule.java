package com.jsmall.saltyfriends.injection.module;

import android.content.Context;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbHelperModule {

    @Provides
    @Singleton
    public DbHelper provideDbHelper(Context context) {
        return new DbHelper(context);
    }
}
