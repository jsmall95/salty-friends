package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.players.PlayerRepository;
import com.jsmall.saltyfriends.data.sq_lite.players.SQLitePlayerRepository;

public class SQLitePlayerRepositoryModule extends PlayerRepositoryModule {

    @Override
    public PlayerRepository providePlayerRepository(DbHelper dbHelper) {
        return provideSQLitePlayerRepository(dbHelper);
    }

    public PlayerRepository provideSQLitePlayerRepository(DbHelper dbHelper){
        return new SQLitePlayerRepository(dbHelper);
    }
}
