package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.domain.bet_state.BetStateAPI;
import com.jsmall.saltyfriends.injection.EmptyModuleException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BetStateServiceModule {
    @Provides
    @Singleton
    public BetStateAPI provideBetStateService(){
        throw new EmptyModuleException();
    }
}
