package com.jsmall.saltyfriends.injection.component;

import com.jsmall.saltyfriends.data.io_socket.BetStateChangedService;
import com.jsmall.saltyfriends.injection.module.DataAccessModule;
import com.jsmall.saltyfriends.ui.bets.EventBetsSummaryFragment;
import com.jsmall.saltyfriends.ui.bets.EventWinnerSummaryFragment;
import com.jsmall.saltyfriends.ui.bets.PlaceBetsFragment;
import com.jsmall.saltyfriends.ui.players.ViewPlayersFragment;
import com.jsmall.saltyfriends.ui.stats.ViewStatsFragment;

import javax.inject.Scope;

import dagger.Component;

@Component(modules = DataAccessModule.class, dependencies = AppComponent.class)
@DataAccessComponent.DataAccessScope
public abstract class DataAccessComponent {

    @Scope
    public @interface DataAccessScope{

    }

    public static DataAccessComponent initialize(){
         return DaggerDataAccessComponent.builder()
                .appComponent(AppComponent.get())
                .build();
    }

    //Fragments
    public abstract void inject(EventBetsSummaryFragment fragment);
    public abstract void inject(PlaceBetsFragment fragment);
    public abstract void inject(EventWinnerSummaryFragment fragment);
    public abstract void inject(ViewPlayersFragment fragment);
    public abstract void inject(ViewStatsFragment fragment);

    //Services
    public abstract void inject(BetStateChangedService service);
}
