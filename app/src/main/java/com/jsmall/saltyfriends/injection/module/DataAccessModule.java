package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.domain.bet_state.BetStateAPI;
import com.jsmall.saltyfriends.domain.bet_state.GetBetStateUseCase;
import com.jsmall.saltyfriends.domain.bets.BetRepository;
import com.jsmall.saltyfriends.domain.bets.ManageBetsUseCase;
import com.jsmall.saltyfriends.domain.events.EventRepository;
import com.jsmall.saltyfriends.domain.events.ManageEventsUseCase;
import com.jsmall.saltyfriends.domain.players.ManagePlayersUseCase;
import com.jsmall.saltyfriends.domain.players.PlayerRepository;

import dagger.Lazy;
import dagger.Module;
import dagger.Provides;

@Module
public class DataAccessModule {

    @Provides
    public static GetBetStateUseCase provideBetStateUseCase(Lazy<BetStateAPI> service){
        return new GetBetStateUseCase(service.get());
    }

    @Provides
    public static ManagePlayersUseCase provideManagePlayersUseCase(Lazy<PlayerRepository> repo){
        return new ManagePlayersUseCase(repo.get());
    }

    @Provides
    public static ManageEventsUseCase provideManageEventUseCase(Lazy<EventRepository> eventRepo, Lazy<PlayerRepository> playerRepo, Lazy<BetRepository> betRepo){
        return new ManageEventsUseCase(eventRepo.get(), playerRepo.get(), betRepo.get());
    }

    @Provides
    public static ManageBetsUseCase provideManageBetUseCase(Lazy<BetRepository> repo){
        return new ManageBetsUseCase(repo.get());
    }
}
