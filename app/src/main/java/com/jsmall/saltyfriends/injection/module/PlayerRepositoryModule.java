package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.players.PlayerRepository;
import com.jsmall.saltyfriends.injection.EmptyModuleException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PlayerRepositoryModule {
    @Provides
    @Singleton
    public PlayerRepository providePlayerRepository(DbHelper dbHelper){
        throw new EmptyModuleException();
    }
}
