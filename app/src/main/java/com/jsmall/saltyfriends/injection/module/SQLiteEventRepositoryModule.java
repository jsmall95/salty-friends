package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.events.EventRepository;
import com.jsmall.saltyfriends.data.sq_lite.events.SQLiteEventRepository;

public class SQLiteEventRepositoryModule extends EventRepositoryModule {

    @Override
    public EventRepository provideEventRepository(DbHelper dbHelper) {
        return provideSQLiteEventRepository(dbHelper);
    }

    public EventRepository provideSQLiteEventRepository(DbHelper dbHelper){
        return new SQLiteEventRepository(dbHelper);
    }
}
