package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.domain.bet_state.BetStateAPI;
import com.jsmall.saltyfriends.data.ok_http.bet_state.OkHttpBetStateAPI;

public class HttpApiBetStateServiceModule extends BetStateServiceModule {
    @Override
    public BetStateAPI provideBetStateService() {
        return provideHttpApiBetStateService();
    }

    public BetStateAPI provideHttpApiBetStateService(){
        return new OkHttpBetStateAPI();
    }
}
