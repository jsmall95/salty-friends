package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.events.EventRepository;
import com.jsmall.saltyfriends.injection.EmptyModuleException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EventRepositoryModule {
    @Provides
    @Singleton
    public EventRepository provideEventRepository(DbHelper dbHelper){
        throw new EmptyModuleException();
    }
}
