package com.jsmall.saltyfriends.injection.module;

import android.content.Context;

import com.jsmall.saltyfriends.injection.EmptyModuleException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    public Context provideAppContext(){
        throw new EmptyModuleException();
    }
}
