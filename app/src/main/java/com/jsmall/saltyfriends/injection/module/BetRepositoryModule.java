package com.jsmall.saltyfriends.injection.module;

import com.jsmall.saltyfriends.data.sq_lite.utils.DbHelper;
import com.jsmall.saltyfriends.domain.bets.BetRepository;
import com.jsmall.saltyfriends.injection.EmptyModuleException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BetRepositoryModule {
    @Provides
    @Singleton
    public BetRepository provideBetRepository(DbHelper dbHelper){
        throw new EmptyModuleException();
    }
}
