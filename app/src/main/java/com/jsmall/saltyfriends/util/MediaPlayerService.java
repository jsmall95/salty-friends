package com.jsmall.saltyfriends.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.IBinder;

import com.jsmall.saltyfriends.R;

import java.util.Calendar;
import java.util.Random;

import androidx.annotation.Nullable;

public class MediaPlayerService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private final static String MP_PREF_KEY = "com.jsmall.saltyfriends.media_player_prefs";
    private final static String MP_LAST_VIEW_KEY = "com.jsmall.saltyfriends.last_view";
    private final static String MP_PLAY_TIME = "com.jsmall.saltyfriends.play_time";
    private final static String MP_SONG_INDEX_KEY = "com.jsmall.saltyfriends.song_index";

    private MediaPlayer _mediaPlayer;
    private int _mediaPlayerCurrentPosition = 0;
    private int _currentSongIndex = -1;

    private SharedPreferences _prefs;

    private int[] _songList = {
            R.raw.track_0,
            R.raw.track_1,
            R.raw.track_2,
            R.raw.track_3,
            R.raw.track_4
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        _prefs = getSharedPreferences(MP_PREF_KEY, Context.MODE_PRIVATE);
        long currentTimeSpan = Calendar.getInstance().getTimeInMillis() - (1000*10*60);
        if(_prefs.getLong(MP_LAST_VIEW_KEY, 0) < currentTimeSpan){
            SharedPreferences.Editor editor = _prefs.edit();
            editor.clear();
            editor.commit();
        } else {
            _currentSongIndex = _prefs.getInt(MP_SONG_INDEX_KEY, getRandomSong());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        processPlayRequest();
        return START_NOT_STICKY;
    }

    private void processPlayRequest(){
        destroyExistingMedia();

        _mediaPlayer = MediaPlayer.create(this, getCurrentSong());
        _mediaPlayer.setOnPreparedListener(this);
        _mediaPlayer.setOnCompletionListener(this);
    }

    private void destroyExistingMedia(){
        if(_mediaPlayer != null){
            if(_mediaPlayer.isPlaying()){
                _mediaPlayer.stop();
            }
            _mediaPlayerCurrentPosition = _mediaPlayer.getCurrentPosition();
            _mediaPlayer.release();
            _mediaPlayer = null;
            writePlayTimePref();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyExistingMedia();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        int savedSongIndex = _prefs.getInt(MP_SONG_INDEX_KEY, -1);
        int startingPoint = 0;
        if(_currentSongIndex == savedSongIndex){
            startingPoint = _prefs.getInt(MP_PLAY_TIME, 0);
        }
        mp.start();
        mp.seekTo(startingPoint);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        _currentSongIndex++;
        processPlayRequest();
    }

    public int getCurrentSong(){
        if(_currentSongIndex > (_songList.length - 1) || _currentSongIndex < 0){
            _currentSongIndex = 0;
        }
        return _songList[_currentSongIndex];
    }

    public int getRandomSong(){
        return new Random().nextInt(_songList.length);
    }

    private void writePlayTimePref(){
        SharedPreferences.Editor editor = _prefs.edit();
        editor.putInt(MP_PLAY_TIME, _mediaPlayerCurrentPosition);
        long currentTime = Calendar.getInstance().getTimeInMillis();
        editor.putLong(MP_LAST_VIEW_KEY, currentTime);
        editor.putInt(MP_SONG_INDEX_KEY, _currentSongIndex);
        editor.commit();
    }
}
