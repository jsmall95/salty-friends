package com.jsmall.saltyfriends.ui.bets;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jsmall.saltyfriends.ui.bets.mappers.WinnerSummaryAdapter;
import com.jsmall.saltyfriends.MainActivity;
import com.jsmall.saltyfriends.injection.component.DataAccessComponent;
import com.jsmall.saltyfriends.domain.events.EventModel;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.domain.events.ManageEventsUseCase;
import com.jsmall.saltyfriends.util.MediaPlayerService;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class EventWinnerSummaryFragment extends Fragment {

    private View _rootView;
    private MainActivity _activity;

    private TextView _finalResultsText;

    private EventModel _lastWinEvent;

    @Inject
    public ManageEventsUseCase manageEventsUseCase;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _rootView = inflater.inflate(R.layout.fragment_event_winner_summary, container, false);
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _activity = (MainActivity)getActivity();
        DataAccessComponent.initialize().inject(this);

        _lastWinEvent = manageEventsUseCase.getLastEvent();
        if(_lastWinEvent == null){
            _activity.navigateFromMenu(R.id.bets_view);
        }

        _finalResultsText = _rootView.findViewById(R.id.final_results_text);
        _finalResultsText.setText(_lastWinEvent.getWinner() + " Wins!");
        _finalResultsText.setTextColor(manageEventsUseCase.getWinnerColor(_lastWinEvent));

        BuildRecyclerViews();

        _activity.disableLoadingPanel();
    }

    public void BuildRecyclerViews(){
        RecyclerView _recyclerView = _rootView.findViewById(R.id.final_results_list);
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager _layoutManager = new LinearLayoutManager(_activity);
        RecyclerView.Adapter _adapter = new WinnerSummaryAdapter(_lastWinEvent.getPlayerBets());
        _recyclerView.setLayoutManager(_layoutManager);
        _recyclerView.setAdapter(_adapter);
    }
}
