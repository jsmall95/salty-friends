package com.jsmall.saltyfriends.ui.bets;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jsmall.saltyfriends.ui.bets.mappers.PlayerBetAdapter;
import com.jsmall.saltyfriends.domain.bet_state.BetStateModel;
import com.jsmall.saltyfriends.domain.bet_state.GetBetStateUseCase;
import com.jsmall.saltyfriends.MainActivity;
import com.jsmall.saltyfriends.domain.bets.BetModel;
import com.jsmall.saltyfriends.domain.bets.ManageBetsUseCase;
import com.jsmall.saltyfriends.injection.component.DataAccessComponent;
import com.jsmall.saltyfriends.domain.events.EventModel;
import com.jsmall.saltyfriends.ui.bets.view_models.SelectedPlayerBetViewModel;
import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.domain.events.ManageEventsUseCase;
import com.jsmall.saltyfriends.domain.players.ManagePlayersUseCase;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.util.MediaPlayerService;

import java.time.Instant;
import java.util.Date;

import javax.inject.Inject;

public class PlaceBetsFragment extends Fragment implements SetBetAmountDialogFragment.SetBetAmountDialogListener {

    private PlayerBetAdapter _adapter;

    private View _rootView;
    private MainActivity _activity;

    private EventModel _activeEvent;

    private SelectedPlayerBetViewModel _model;

    private boolean _fragmentInitialized = false;

    private BroadcastReceiver _receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getCurrentBetState();
        }
    };

    private CountDownTimer _countDown;

    @Inject
    public ManagePlayersUseCase managePlayersUseCase;

    @Inject
    public ManageEventsUseCase manageEventsUseCase;

    @Inject
    public ManageBetsUseCase manageBetsUseCase;

    @Inject
    public GetBetStateUseCase getBetStateUseCase;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _rootView = inflater.inflate(R.layout.fragment_place_bets, container, false);

        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        _activity = (MainActivity)getActivity();
        DataAccessComponent.initialize().inject(this);

        _model = ViewModelProviders.of(_activity).get(SelectedPlayerBetViewModel.class);

        InitializeActiveEvent();
        getCurrentBetState();
    }

    @Override
    public void onResume() {
        super.onResume();
        _activity.registerReceiver(_receiver, new IntentFilter(getString(R.string.update_current_bet_state)));
    }

    @Override
    public void onPause() {
        super.onPause();
        _activity.unregisterReceiver(_receiver);
    }

    public void BuildRecycleView(){
        RecyclerView _recyclerView = _rootView.findViewById(R.id.playerBetsRecyclerView);
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager _layoutManager = new LinearLayoutManager(_activity);
        _adapter = new PlayerBetAdapter(_activeEvent.getPlayerBets());

        _recyclerView.setLayoutManager(_layoutManager);
        _recyclerView.setAdapter(_adapter);

        _adapter.setOnPlayerBetClickListener(new PlayerBetAdapter.OnPlayerBetClickListener() {
            @Override
            public void onVoteRedClick(int position) {
                openSetBetDialog(position, _activeEvent.getRedPlayer(), BetModel.BET_COLORS.Red.toString());
            }

            @Override
            public void onVoteBlueClick(int position) {
                openSetBetDialog(position, _activeEvent.getBluePlayer(), BetModel.BET_COLORS.Blue.toString());
            }
        });
    }

    public void InitializeActiveEvent(){
        _activeEvent = manageEventsUseCase.getActiveEvent();
        if(_activeEvent.isFinalized()){
            showEventSummary();
        }
    }

    public void InitializeFight(BetStateModel betState){
        final TextView timerText = _rootView.findViewById(R.id.place_bets_timer);
        _countDown= new CountDownTimer( 55000 - (Date.from(Instant.now()).getTime() - betState.getTime()), 1000){

            @Override
            public void onTick(long millisUntilFinished) {
                timerText.setText(Long.toString(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                timerText.setText("GO!");
            }
        };

        TextView redPlayer = _rootView.findViewById(R.id.place_bets_red_player);
        redPlayer.setText(_activeEvent.getRedPlayer());
        redPlayer.setTextColor(BetModel.BET_COLORS.Red.getColorCode());
        TextView bluePlayer = _rootView.findViewById(R.id.place_bets_blue_player);
        bluePlayer.setText(_activeEvent.getBluePlayer());
        bluePlayer.setTextColor(BetModel.BET_COLORS.Blue.getColorCode());

        _countDown.start();
    }

    private void openSetBetDialog(int position, String betValue, String betColor){
        PlayerBetModel currentPlayerBet = _activeEvent.getPlayerBets().get(position);
        if(currentPlayerBet != null){
            currentPlayerBet.getBet().changeBetValue(betValue);
            currentPlayerBet.getBet().setBetColor(betColor);
            _model.select(currentPlayerBet, position);
            DialogFragment setBetDialog = new SetBetAmountDialogFragment();
            setBetDialog.setTargetFragment(this, 0);
            setBetDialog.show(getFragmentManager().beginTransaction(), "placeBetDialog");
        }
    }

    private void finalizeBets(){
        if(_activeEvent != null){
            _activeEvent.setFinalized(true);
            if(manageEventsUseCase.updateEvent(_activeEvent) != null){
                showEventSummary();
            }
            else{
                Toast.makeText(_activity, "Something went wrong, try again.", Toast.LENGTH_LONG).show();
            }
        } else {
            // tell player to wait till fight ends
        }
    }

    private void showEventSummary(){
        Bundle bundle = new Bundle();
        bundle.putParcelable(MainActivity.ACTIVE_EVENT, _activeEvent);
        Fragment eventBetsSummary = new EventBetsSummaryFragment();
        eventBetsSummary.setArguments(bundle);
        _activity.loadFragment(eventBetsSummary, MainActivity.BETS_VIEW, true);
    }

    private void getCurrentBetState(){
        BetStateModel betState = getBetStateUseCase.getCurrentBetState();
        if(betState != null){
            String betStateStatus = getBetStateUseCase.getBetStateStatus();
            if(betStateStatus.equals(BetStateModel.BET_STATE_STATUS.locked.toString()) && _fragmentInitialized){
                finalizeBets();
            } else if (betStateStatus.equals(BetStateModel.BET_STATE_STATUS.open.toString()) && !_fragmentInitialized){
                if(_activeEvent != null){
                    _activeEvent.setRedPlayer(betState.getP1Name());
                    _activeEvent.setBluePlayer(betState.getP2Name());
                    manageEventsUseCase.updateEvent(_activeEvent);
                    BuildRecycleView();
                    InitializeFight(betState);
                    _activity.disableLoadingPanel();
                    _fragmentInitialized = true;
                }
            }
        }
    }

    @Override
    public void onDialogPositiveClick() {
        if(manageBetsUseCase.updateBet(_model.getSelected().getBet()) != null){
            _adapter.notifyItemChanged(_model.getSelectedPosition());
        }
        _model.clearSelected();
    }

    @Override
    public void onDialogNegativeClick() {
        _model.clearSelected();
    }
}
