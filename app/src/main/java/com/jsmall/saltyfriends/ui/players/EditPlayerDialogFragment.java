package com.jsmall.saltyfriends.ui.players;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.jsmall.saltyfriends.domain.players.PlayerModel;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.ui.players.view_models.SelectedPlayerViewModel;


public class EditPlayerDialogFragment extends DialogFragment {

    private EditText _playerName;
    private EditText _playerTotal;

    private View _dialogView;

    private SelectedPlayerViewModel _model;

    public interface EditPlayerDialogListener {
        void onDialogPositiveClick();
        void onDialogNegativeClick();
    }

    private EditPlayerDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        _dialogView = inflater.inflate(R.layout.fragment_edit_player_dialog, null);

        builder.setView(_dialogView)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        _model.getSelected().setPlayerName(_playerName.getText().toString());
                        _model.getSelected().setTotalCredits(Integer.parseInt(_playerTotal.getText().toString()));
                        listener.onDialogPositiveClick();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogNegativeClick();
                    }
                });

        _model = ViewModelProviders.of(getActivity()).get(SelectedPlayerViewModel.class);

        InitializeDialog();

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener = (EditPlayerDialogFragment.EditPlayerDialogListener)getTargetFragment();
        } catch( ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement SetBetAmountDialogListener");
        }
    }

    private void InitializeDialog(){
        PlayerModel player = _model.getSelected();
        if(player != null){
            _playerName = _dialogView.findViewById(R.id.edit_player_name);
            _playerName.setText(_model.getSelected().getPlayerName());
            _playerTotal = _dialogView.findViewById(R.id.edit_player_total);
            _playerTotal.setText(Integer.toString(_model.getSelected().getTotalCredits()));
        }
    }
}
