package com.jsmall.saltyfriends.ui.players;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jsmall.saltyfriends.ui.players.mappers.PlayerAdapter;
import com.jsmall.saltyfriends.MainActivity;
import com.jsmall.saltyfriends.domain.bets.ManageBetsUseCase;
import com.jsmall.saltyfriends.injection.component.DataAccessComponent;
import com.jsmall.saltyfriends.domain.players.ManagePlayersUseCase;
import com.jsmall.saltyfriends.domain.players.PlayerModel;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.ui.players.view_models.SelectedPlayerViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class ViewPlayersFragment extends Fragment implements View.OnClickListener, EditPlayerDialogFragment.EditPlayerDialogListener {

    private static final String STATE_PLAYER_LIST = "Player List Data";

    private ArrayList<PlayerModel> _playerList;

    private PlayerAdapter _adapter;

    private View _rootView;
    private MainActivity _activity;

    @Inject
    public ManagePlayersUseCase managePlayersUseCase;

    @Inject
    public ManageBetsUseCase manageBetsUseCase;

    private SelectedPlayerViewModel _model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _rootView = inflater.inflate(R.layout.fragment_view_players, container, false);
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _activity = (MainActivity)getActivity();
        DataAccessComponent.initialize().inject(this);
        _playerList = managePlayersUseCase.getPlayers();

        _model = ViewModelProviders.of(_activity).get(SelectedPlayerViewModel.class);

        Button addPlayerButton = _activity.findViewById(R.id.addPlayerButton);
        addPlayerButton.setOnClickListener(this);

        BuildRecycleView();
        _activity.disableLoadingPanel();
    }

    public void BuildRecycleView(){
        RecyclerView _recyclerView = _rootView.findViewById(R.id.playerRecyclerView);
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager _layoutManager = new LinearLayoutManager(_activity);
        _adapter = new PlayerAdapter(_playerList);

        _recyclerView.setLayoutManager(_layoutManager);
        _recyclerView.setAdapter(_adapter);

        _adapter.setOnPlayerClickListener(new PlayerAdapter.OnPlayerClickListener() {
            @Override
            public void onPlayerClick(int position) {
                openEditPlayerDialog(position);
            }

            @Override
            public void onDeleteClick(int position) {
                removePlayer(position);
            }
        });
    }

    public void removePlayer(int position){
        PlayerModel player = _playerList.get(position);
        if(managePlayersUseCase.deletePlayer(player)){
            manageBetsUseCase.deletePlayerBets(player.getId());
            _playerList.remove(position);
            _adapter.notifyItemRemoved(position);
        }
    }

    public void addPlayer(View view){
        EditText editText = _rootView.findViewById(R.id.newPlayerNameText);
        if(editText != null){
            String newPlayerName = editText.getText().toString();
            if(validateNewPlayerName(newPlayerName)){
                int imageId = _activity.getResources().getIdentifier("ic_salt_" + ((_playerList.size() % 8) + 1), "drawable", _activity.getPackageName());
                PlayerModel newPlayer = managePlayersUseCase.addPlayer(newPlayerName, imageId, 500);
                if(newPlayer != null){
                    _playerList.add(newPlayer);
                    _adapter.notifyItemInserted(_playerList.size() - 1);
                    editText.setText("");
                    editText.clearFocus();
                }
            }
        }
    }

    private boolean validateNewPlayerName(String newPlayerName){
        if(newPlayerName != null && !newPlayerName.isEmpty()){
            for (PlayerModel player: _playerList){
                if(player.getPlayerName().equals(newPlayerName)){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void openEditPlayerDialog(int position){
        PlayerModel currentPlayer = _playerList.get(position);
        _model.select(currentPlayer, position);
        DialogFragment editPlayerDialog = new EditPlayerDialogFragment();
        editPlayerDialog.setTargetFragment(this, 0);
        editPlayerDialog.show(getFragmentManager().beginTransaction(), "editPlayerDialog");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.addPlayerButton) {
            addPlayer(v);
        }
    }

    @Override
    public void onDialogPositiveClick() {
        PlayerModel player = _model.getSelected();
        if(player != null){
            managePlayersUseCase.updatePlayer(_model.getSelected());
            _adapter.notifyItemChanged(_model.getSelectedPosition());
            _model.clearSelected();
        }
    }

    @Override
    public void onDialogNegativeClick() {
        _model.clearSelected();
    }
}
