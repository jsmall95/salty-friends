package com.jsmall.saltyfriends.ui.players.view_models;

import com.jsmall.saltyfriends.domain.players.PlayerModel;

import androidx.lifecycle.ViewModel;

public class SelectedPlayerViewModel extends ViewModel {
    private  PlayerModel _selectedPlayer;
    private int _selectedPosition;

    public void select(PlayerModel player, int position){
        _selectedPlayer = player;
        _selectedPosition = position;
    }

    public PlayerModel getSelected(){
        return _selectedPlayer;
    }

    public int getSelectedPosition(){
        return _selectedPosition;
    }

    public void clearSelected(){
        _selectedPosition = -1;
        _selectedPlayer = null;
    }
}
