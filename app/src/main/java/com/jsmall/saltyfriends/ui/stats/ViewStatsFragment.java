package com.jsmall.saltyfriends.ui.stats;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jsmall.saltyfriends.ui.stats.mappers.PlayerStatsAdapter;
import com.jsmall.saltyfriends.MainActivity;
import com.jsmall.saltyfriends.domain.bets.BetModel;
import com.jsmall.saltyfriends.domain.bets.ManageBetsUseCase;
import com.jsmall.saltyfriends.injection.component.DataAccessComponent;
import com.jsmall.saltyfriends.domain.players.ManagePlayersUseCase;
import com.jsmall.saltyfriends.domain.players.PlayerModel;
import com.jsmall.saltyfriends.domain.stats.PlayerStatsModel;
import com.jsmall.saltyfriends.R;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ViewStatsFragment extends Fragment {

    private View _rootView;
    private MainActivity _activity;

    private ArrayList<PlayerStatsModel> _playerStats;

    @Inject
    public ManagePlayersUseCase managePlayersUseCase;

    @Inject
    public ManageBetsUseCase manageBetsUseCase;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _rootView = inflater.inflate(R.layout.fragment_view_stats, container, false);
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _activity = (MainActivity)getActivity();
        DataAccessComponent.initialize().inject(this);

        InitializePlayerStats();
        BuildRecyclerViews();

        _activity.disableLoadingPanel();
    }

    public void BuildRecyclerViews(){
        RecyclerView _recyclerView = _rootView.findViewById(R.id.player_stats_list);
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager _layoutManager = new LinearLayoutManager(_activity);
        RecyclerView.Adapter _adapter = new PlayerStatsAdapter(_playerStats);
        _recyclerView.setLayoutManager(_layoutManager);
        _recyclerView.setAdapter(_adapter);
    }

    private void InitializePlayerStats(){
        _playerStats = new ArrayList<PlayerStatsModel>();
        ArrayList<PlayerModel> players = managePlayersUseCase.getPlayers();
        for (PlayerModel player: players) {
            int totalWins = 0;
            int totalLosses = 0;
            ArrayList<BetModel> bets = manageBetsUseCase.getPlayerBets(player.getId());
            for(BetModel bet: bets){
                if (bet.getTotalWinnings() > 0 && !bet.getActive()){
                    totalWins++;
                }
                else if(!bet.getActive()){
                    totalLosses++;
                }
            }
            _playerStats.add(new PlayerStatsModel(player, totalWins, totalLosses));
        }
    }
}
