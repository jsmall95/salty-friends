package com.jsmall.saltyfriends.ui.bets.mappers;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerBetAdapter extends RecyclerView.Adapter<PlayerBetAdapter.PlayerViewHolder> {
    private ArrayList<PlayerBetModel> _playerBetList;
    private PlayerBetAdapter.OnPlayerBetClickListener _listener;

    public interface OnPlayerBetClickListener{
        void onVoteRedClick(int position);
        void onVoteBlueClick(int position);
    }

    public void setOnPlayerBetClickListener(PlayerBetAdapter.OnPlayerBetClickListener listener){
        _listener = listener;
    }

    public static class PlayerViewHolder extends RecyclerView.ViewHolder{
        public ImageView _playerImage;
        public TextView _playerName;
        public TextView _playerTotal;
        public ImageView _voteRedImage;
        public ImageView _voteBlueImage;

        public PlayerViewHolder(@NonNull View itemView, final PlayerBetAdapter.OnPlayerBetClickListener listener) {
            super(itemView);
            _playerImage = itemView.findViewById(R.id.player_bet_image);
            _playerName = itemView.findViewById(R.id.player_bet_name);
            _playerTotal = itemView.findViewById(R.id.player_bet_total);
            _voteRedImage = itemView.findViewById(R.id.image_red_vote);
            _voteBlueImage = itemView.findViewById(R.id.image_blue_vote);

            _voteRedImage.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onVoteRedClick(position);
                        }
                    }
                }
            });

            _voteBlueImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onVoteBlueClick(position);
                        }
                    }
                }
            });
        }
    }

    public PlayerBetAdapter(ArrayList<PlayerBetModel> playerBetList){
        _playerBetList = playerBetList;
    }

    @NonNull
    @Override
    public PlayerBetAdapter.PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_bet_item, parent, false);
        return new PlayerViewHolder(v, _listener);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerBetAdapter.PlayerViewHolder holder, int position) {
        PlayerBetModel currentPlayerBet = _playerBetList.get(position);

        if(currentPlayerBet != null){
            holder._playerImage.setImageResource(currentPlayerBet.getPlayer().getImageResource());
            holder._playerImage.setImageTintList(ColorStateList.valueOf(currentPlayerBet.getBet().getBetColor()));
            holder._playerName.setText(currentPlayerBet.getPlayer().getPlayerName());
            holder._playerName.setTextColor(currentPlayerBet.getBet().getBetColor());
            holder._playerTotal.setText(Integer.toString(currentPlayerBet.getBet().getBetAmount()));
        }
    }

    @Override
    public int getItemCount() {
        return _playerBetList.size();
    }

    public ArrayList<PlayerBetModel> getList(){
        return new ArrayList<PlayerBetModel>(_playerBetList);
    }
}
