package com.jsmall.saltyfriends.ui.bets.view_models;

import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;

import androidx.lifecycle.ViewModel;

public class SelectedPlayerBetViewModel extends ViewModel {

    private PlayerBetModel _selectedPlayerBet;
    private int _selectedPosition;

    public void select(PlayerBetModel playerBet, int position){
        _selectedPlayerBet = playerBet;
        _selectedPosition = position;
    }

    public PlayerBetModel getSelected(){ return _selectedPlayerBet; }

    public int getSelectedPosition() { return _selectedPosition; }

    public void clearSelected(){
        _selectedPosition = -1;
        _selectedPlayerBet = null;
    }
}
