package com.jsmall.saltyfriends.ui.bets.mappers;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BetSummaryAdapter extends RecyclerView.Adapter<BetSummaryAdapter.PlayerViewHolder> {
    private ArrayList<PlayerBetModel> _playerBetList;

    public static class PlayerViewHolder extends RecyclerView.ViewHolder{
        private ImageView _playerImage;
        private TextView _playerName;
        private TextView _playerBetAmount;
        private TextView _playerBetGain;

        public PlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            _playerImage = itemView.findViewById(R.id.bet_summary_image);
            _playerName = itemView.findViewById(R.id.bet_summary_name);
            _playerBetAmount = itemView.findViewById(R.id.bet_summary_amount);
        }
    }

    public BetSummaryAdapter(ArrayList<PlayerBetModel> playerBetList){
        _playerBetList = playerBetList;
    }

    @NonNull
    @Override
    public BetSummaryAdapter.PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bet_summary_item, parent, false);
        return new PlayerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BetSummaryAdapter.PlayerViewHolder holder, int position) {
        PlayerBetModel currentPlayerBet = _playerBetList.get(position);

        if(currentPlayerBet != null){
            holder._playerImage.setImageResource(currentPlayerBet.getPlayer().getImageResource());
            holder._playerImage.setImageTintList(ColorStateList.valueOf(currentPlayerBet.getBet().getBetColor()));
            holder._playerName.setText(currentPlayerBet.getPlayer().getPlayerName());
            holder._playerName.setTextColor(currentPlayerBet.getBet().getBetColor());
            holder._playerBetAmount.setTextColor(currentPlayerBet.getBet().getBetAmount());
        }
    }

    @Override
    public int getItemCount() {
        return _playerBetList.size();
    }

    public ArrayList<PlayerBetModel> getList(){
        return new ArrayList<PlayerBetModel>(_playerBetList);
    }
}
