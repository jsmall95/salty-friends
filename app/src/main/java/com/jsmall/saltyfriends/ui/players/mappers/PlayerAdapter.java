package com.jsmall.saltyfriends.ui.players.mappers;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsmall.saltyfriends.domain.players.PlayerModel;
import com.jsmall.saltyfriends.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder> {

    private ArrayList<PlayerModel> _playerList;
    private OnPlayerClickListener _listener;

    public interface OnPlayerClickListener{
        void onPlayerClick(int position);
        void onDeleteClick(int position);
    }

    public void setOnPlayerClickListener(OnPlayerClickListener listener){
        _listener = listener;
    }

    public static class PlayerViewHolder extends RecyclerView.ViewHolder{
        public ImageView _imageView;
        public TextView _playerName;
        public TextView _playerTotal;
        public ImageView _deleteImage;

        public PlayerViewHolder(@NonNull View itemView, final OnPlayerClickListener listener) {
            super(itemView);
            _imageView = itemView.findViewById(R.id.player_image);
            _playerName = itemView.findViewById(R.id.player_name_text);
            _playerTotal = itemView.findViewById(R.id.player_total_text);
            _deleteImage = itemView.findViewById(R.id.image_delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onPlayerClick(position);
                        }
                    }
                }
            });

            _deleteImage.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public PlayerAdapter(ArrayList<PlayerModel> playerList){
        _playerList = playerList;
    }

    @NonNull
    @Override
    public PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_item, parent, false);
        return new PlayerViewHolder(v, _listener);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerViewHolder holder, int position) {
        PlayerModel currentPlayer = _playerList.get(position);

        holder._imageView.setImageResource(currentPlayer.getImageResource());
        holder._imageView.setImageTintList(ColorStateList.valueOf(Color.WHITE));
        holder._playerName.setText(currentPlayer.getPlayerName());
        holder._playerTotal.setText(Integer.toString(currentPlayer.getTotalCredits()));
    }

    @Override
    public int getItemCount() {
        return _playerList.size();
    }

    public ArrayList<PlayerModel> getList(){
        return new ArrayList<PlayerModel>(_playerList);
    }

}
