package com.jsmall.saltyfriends.ui.bets;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.ui.bets.view_models.SelectedPlayerBetViewModel;

public class SetBetAmountDialogFragment extends DialogFragment {

    private SeekBar _seekBar;
    private TextView _seekText;

    private View _dialogView;

    private SelectedPlayerBetViewModel _model;

    public interface SetBetAmountDialogListener {
        void onDialogPositiveClick();
        void onDialogNegativeClick();
    }

    SetBetAmountDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        _dialogView = inflater.inflate(R.layout.fragment_set_bet_amount_dialog, null);

        builder.setView(_dialogView)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogPositiveClick();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogNegativeClick();
                    }
                });

        _model = ViewModelProviders.of(getActivity()).get(SelectedPlayerBetViewModel.class);

        InitializeDialog();
        InitializeBetButtons();

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener = (SetBetAmountDialogListener)getTargetFragment();
        } catch( ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement SetBetAmountDialogListener");
        }
    }

    private void InitializeDialog(){
        PlayerBetModel playerBet = _model.getSelected();

        TextView _betValueText = _dialogView.findViewById(R.id.set_bed_value);
        _betValueText.setText(playerBet.getBet().getBetValue());
        _seekBar = _dialogView.findViewById(R.id.bet_amount_bar);
        _seekText = _dialogView.findViewById(R.id.bet_amount_text);
        int _minBet = 5;
        _seekText.setText("Bet Amount: " + _minBet);
        _seekBar.setMin(_minBet);
        _seekBar.setMax(playerBet.getPlayer().getTotalCredits());
        _seekBar.setProgress(_minBet);

        _seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _seekText.setText("Bet Amount: " + progress);
                _model.getSelected().getBet().setBetAmount(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void InitializeBetButtons(){
        _dialogView.findViewById(R.id.bet_5_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProgressBetAmount(5);
            }
        });

        _dialogView.findViewById(R.id.bet_25_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProgressBetAmount(25);
            }
        });

        _dialogView.findViewById(R.id.bet_100_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProgressBetAmount(100);
            }
        });

        _dialogView.findViewById(R.id.bet_500_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProgressBetAmount(500);
            }
        });
    }

    private void setProgressBetAmount(int betAmount){
        _seekBar.setProgress(betAmount);
    }
}
