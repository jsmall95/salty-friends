package com.jsmall.saltyfriends.ui.bets.mappers;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModelComparator;
import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.R;

import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class WinnerSummaryAdapter extends RecyclerView.Adapter<WinnerSummaryAdapter.PlayerViewHolder> {
    private ArrayList<PlayerBetModel> _playerBetList;

    public static class PlayerViewHolder extends RecyclerView.ViewHolder{
        public TextView _playerRank;
        public ImageView _playerImage;
        public TextView _playerName;
        public TextView _playerEarnings;
        public TextView _playerDifference;
        public TextView _playerTotal;

        public PlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            _playerRank = itemView.findViewById(R.id.player_winner_rank);
            _playerImage = itemView.findViewById(R.id.player_winner_image);
            _playerName = itemView.findViewById(R.id.player_winner_name);
            _playerEarnings = itemView.findViewById(R.id.player_winner_earnings);
            _playerDifference = itemView.findViewById(R.id.player_winner_difference);
            _playerTotal = itemView.findViewById(R.id.player_winner_total);
        }
    }

    public WinnerSummaryAdapter(ArrayList<PlayerBetModel> playerBetList){
        Collections.sort(playerBetList, new PlayerBetModelComparator());
        _playerBetList = playerBetList;
    }

    @NonNull
    @Override
    public WinnerSummaryAdapter.PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.winner_summary_item, parent, false);
        return new PlayerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WinnerSummaryAdapter.PlayerViewHolder holder, int position) {
        PlayerBetModel currentPlayerBet = _playerBetList.get(position);

        if(currentPlayerBet != null){
            holder._playerRank.setText(Integer.toString(position + 1));
            holder._playerImage.setImageResource(currentPlayerBet.getPlayer().getImageResource());
            holder._playerImage.setImageTintList(ColorStateList.valueOf(currentPlayerBet.getBet().getBetColor()));
            holder._playerName.setText(currentPlayerBet.getPlayer().getPlayerName());
            holder._playerName.setTextColor(currentPlayerBet.getBet().getBetColor());

            int totalWinnings = currentPlayerBet.getBet().getTotalWinnings();
            holder._playerEarnings.setText(Integer.toString(totalWinnings > 0 ? totalWinnings + currentPlayerBet.getBet().getBetAmount() : 0));
            holder._playerDifference.setText("(" + totalWinnings + ")");
            holder._playerDifference.setTextColor(totalWinnings > 0 ? Color.GREEN : Color.RED);
            holder._playerTotal.setText(Integer.toString(currentPlayerBet.getPlayer().getTotalCredits()));
        }
    }

    @Override
    public int getItemCount() {
        return _playerBetList.size();
    }

    public ArrayList<PlayerBetModel> getList(){
        return new ArrayList<PlayerBetModel>(_playerBetList);
    }
}
