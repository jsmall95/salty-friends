package com.jsmall.saltyfriends.ui.stats.mappers;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsmall.saltyfriends.domain.stats.PlayerStatsModel;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.domain.stats.PlayerStatsModelComparator;

import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerStatsAdapter extends RecyclerView.Adapter<PlayerStatsAdapter.PlayerViewHolder> {
    private ArrayList<PlayerStatsModel> _playerStatsList;

    public static class PlayerViewHolder extends RecyclerView.ViewHolder{
        public TextView _playerRank;
        public ImageView _playerImage;
        public TextView _playerName;
        public TextView _playerTotalWins;
        public TextView _playerTotalLosses;
        public TextView _playerTotal;

        public PlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            _playerRank = itemView.findViewById(R.id.player_stat_rank);
            _playerImage = itemView.findViewById(R.id.player_stat_image);
            _playerName = itemView.findViewById(R.id.player_stat_name);
            _playerTotalWins = itemView.findViewById(R.id.player_stat_wins);
            _playerTotalLosses = itemView.findViewById(R.id.player_stat_losses);
            _playerTotal = itemView.findViewById(R.id.player_stat_total);

        }
    }

    public PlayerStatsAdapter(ArrayList<PlayerStatsModel> playerStatsList){
        Collections.sort(playerStatsList, new PlayerStatsModelComparator());
        _playerStatsList = playerStatsList;
    }

    @NonNull
    @Override
    public PlayerStatsAdapter.PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_stats, parent, false);
        return new PlayerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerStatsAdapter.PlayerViewHolder holder, int position) {
        PlayerStatsModel currentPlayerStats = _playerStatsList.get(position);

        if(currentPlayerStats != null){
            holder._playerRank.setText(Integer.toString(position + 1));
            holder._playerImage.setImageResource(currentPlayerStats.getPlayer().getImageResource());
            holder._playerImage.setImageTintList(ColorStateList.valueOf(Color.WHITE));
            holder._playerName.setText(currentPlayerStats.getPlayer().getPlayerName());
            holder._playerTotalWins.setText(Integer.toString(currentPlayerStats.getTotalWins()));
            holder._playerTotalLosses.setText(Integer.toString(currentPlayerStats.getTotalLosses()));
            holder._playerTotal.setText(Integer.toString(currentPlayerStats.getPlayer().getTotalCredits()));
        }
    }

    @Override
    public int getItemCount() {
        return _playerStatsList.size();
    }

    public ArrayList<PlayerStatsModel> getList(){
        return new ArrayList<PlayerStatsModel>(_playerStatsList);
    }
}
