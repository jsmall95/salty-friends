package com.jsmall.saltyfriends.ui.bets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jsmall.saltyfriends.ui.bets.mappers.BetSummaryAdapter;
import com.jsmall.saltyfriends.domain.bet_state.GetBetStateUseCase;
import com.jsmall.saltyfriends.MainActivity;
import com.jsmall.saltyfriends.domain.bets.ManageBetsUseCase;
import com.jsmall.saltyfriends.domain.events.EventModel;
import com.jsmall.saltyfriends.domain.player_bets.PlayerBetModel;
import com.jsmall.saltyfriends.R;
import com.jsmall.saltyfriends.injection.component.DataAccessComponent;
import com.jsmall.saltyfriends.domain.events.ManageEventsUseCase;
import com.jsmall.saltyfriends.domain.players.ManagePlayersUseCase;
import com.jsmall.saltyfriends.util.MediaPlayerService;

import java.util.ArrayList;

import javax.inject.Inject;


public class EventBetsSummaryFragment extends Fragment {

    private View _rootView;
    private MainActivity _activity;

    private EventModel _activeEvent;
    private ArrayList<PlayerBetModel> _redPlayers;
    private ArrayList<PlayerBetModel> _bluePlayers;

    private float _redOdds = 0;
    private float _blueOdds = 0;

    private TextView _redFighterName;
    private TextView _blueFighterName;

    private BroadcastReceiver _receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getBetStateWinner();
        }
    };

    @Inject
    public GetBetStateUseCase getBetStateUseCase;

    @Inject
    public ManagePlayersUseCase managePlayersUseCase;

    @Inject
    public ManageEventsUseCase manageEventsUseCase;

    @Inject
    public ManageBetsUseCase manageBetsUseCase;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _rootView = inflater.inflate(R.layout.fragment_event_bets_summary, container, false);
        _activeEvent = getArguments().getParcelable(MainActivity.ACTIVE_EVENT);
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _activity = (MainActivity)getActivity();
        DataAccessComponent.initialize().inject(this);

        InitializeFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        _activity.registerReceiver(_receiver, new IntentFilter(getString(R.string.update_last_win_bet_state)));
    }

    @Override
    public void onPause() {
        super.onPause();
        _activity.unregisterReceiver(_receiver);
    }

    private void InitializeFragment(){
        InitializeEventSummary();
        InitializePlayerBets();
        BuildRecyclerViews();

        _activity.disableLoadingPanel();
    }

    private void BuildRecyclerViews(){
        RecyclerView _redRecyclerView = _rootView.findViewById(R.id.red_bets_list);
        _redRecyclerView.setHasFixedSize(true);
        RecyclerView _blueRecyclerView = _rootView.findViewById(R.id.blue_bets_list);
        _blueRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager _redLayoutManager = new LinearLayoutManager(_activity);
        RecyclerView.LayoutManager _blueLayoutManger = new LinearLayoutManager(_activity);

        RecyclerView.Adapter _redAdapter = new BetSummaryAdapter(_redPlayers);
        RecyclerView.Adapter _blueAdapter = new BetSummaryAdapter(_bluePlayers);

        _redRecyclerView.setLayoutManager(_redLayoutManager);
        _redRecyclerView.setAdapter(_redAdapter);
        _blueRecyclerView.setLayoutManager(_blueLayoutManger);
        _blueRecyclerView.setAdapter(_blueAdapter);
    }

    private void InitializeEventSummary(){
        _redFighterName = _rootView.findViewById(R.id.red_fighter_name);
        _blueFighterName = _rootView.findViewById(R.id.blue_fighter_name);
        _redFighterName.setText(_activeEvent.getRedPlayer());
        _blueFighterName.setText(_activeEvent.getBluePlayer());
        if(_activeEvent.isFinalized() && (_activeEvent.getWinner() != null && !_activeEvent.getWinner().isEmpty())){
            showWinnerSummary();
        }
    }

    private void InitializePlayerBets(){
        _redPlayers = new ArrayList<>();
        _bluePlayers = new ArrayList<>();
        int redTotal = 0;
        int blueTotal = 0;

        for(PlayerBetModel playerBet : _activeEvent.getPlayerBets()){
            if(_activeEvent.getRedPlayer().equals(playerBet.getBet().getBetValue())) {
                _redPlayers.add(playerBet);
                redTotal += playerBet.getBet().getBetAmount();
            }else if(_activeEvent.getBluePlayer().equals(playerBet.getBet().getBetValue())){
                _bluePlayers.add(playerBet);
                blueTotal += playerBet.getBet().getBetAmount();
            }
        }

        int lowestTotal = redTotal < blueTotal ? redTotal : blueTotal;
        if(lowestTotal > 0){
            _redOdds = (float)redTotal / lowestTotal;
            _blueOdds = (float)blueTotal / lowestTotal;
        }
        else{
            _redOdds = 1;
            _blueOdds = 1;
        }

        ((TextView)_rootView.findViewById(R.id.red_odds_text)).setText(String.format("%.2g%n", _redOdds));
        ((TextView)_rootView.findViewById(R.id.blue_odds_text)).setText(String.format("%.2g%n", _blueOdds));
    }

    private void setWinner(String winner){
        _activeEvent.setWinner(winner);
        _activeEvent.setActive(false);
        for(PlayerBetModel playerBet: _activeEvent.getPlayerBets()){
            int betAmount = playerBet.getBet().getBetAmount();
            int totalWinnings = 0;
            if(playerBet.getBet().getBetValue().equals(winner)){
                totalWinnings = (int)(betAmount * getWinningOdds(winner));
            }
            else{
                totalWinnings -= betAmount;
            }

            playerBet.getBet().setTotalWinnings(totalWinnings);
            manageBetsUseCase.updateBet(playerBet.getBet());
            playerBet.getPlayer().addTotalCredits(totalWinnings);
            managePlayersUseCase.updatePlayer(playerBet.getPlayer());
        }
        manageEventsUseCase.updateEvent(_activeEvent);
        showWinnerSummary();
    }

    private void showWinnerSummary(){
        _activity.navigateFromMenu(R.id.last_result_view);
    }

    private float getWinningOdds(String winner){
        if(winner == _activeEvent.getRedPlayer()){
            return _blueOdds / _redOdds;
        }
        else{
            return _redOdds / _blueOdds;
        }
    }

    private void getBetStateWinner(){
        String betStateWinner = getBetStateUseCase.getBetStateWinner();
        if(betStateWinner != null && !betStateWinner.isEmpty()){
            setWinner(betStateWinner);
        }
    }
}
