package com.jsmall.saltyfriends.application;

import android.app.Application;
import android.content.Context;

import com.jsmall.saltyfriends.injection.component.AppComponent;
import com.jsmall.saltyfriends.injection.module.AppModule;
import com.jsmall.saltyfriends.injection.module.DbHelperModule;
import com.jsmall.saltyfriends.injection.module.HttpApiBetStateServiceModule;
import com.jsmall.saltyfriends.injection.module.SQLiteBetRepositoryModule;
import com.jsmall.saltyfriends.injection.module.SQLiteEventRepositoryModule;
import com.jsmall.saltyfriends.injection.module.SQLitePlayerRepositoryModule;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        AppModule appModule = new AppModule() {
            @Override
            public Context provideAppContext() {
                return App.this;
            }
        };

        AppComponent.initialize(
                appModule,
                new HttpApiBetStateServiceModule(),
                new DbHelperModule(),
                new SQLitePlayerRepositoryModule(),
                new SQLiteEventRepositoryModule(),
                new SQLiteBetRepositoryModule());
    }
}
